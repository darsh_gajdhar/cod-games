import React from "react";
import { makeStore } from "../../store/configureStore";
import Head from "next/head";

// import { getShooterByName } from "../../store/api/shooter";
import { props } from "../../interfaces/interfaces";
import GameContainer from "../../src/modules/games/GameContainer";

const ShooterGames = ({ data }: props) => {
  console.log("data", data);
  return (
    <>
      <Head>
        <title>Shooter Games</title>
        <meta name="description" content="next js shooter games" />
      </Head>
      <GameContainer data={data} />
    </>
  );
};

export default ShooterGames;

// export const gameData = {
//   method: "GET",

//   headers: {
//     "X-RapidAPI-Key": "c3176a7de4msh2da1c84eb21b242p1cabb0jsn8549a1515f34",
//     "X-RapidAPI-Host": "free-to-play-games-database.p.rapidapi.com",
//   },
// };

export async function getServerSideProps() {
  // const store = makeStore();
  // const data = await store.dispatch(getShooterByName.initiate());
  const res = await fetch(`https://www.freetogame.com/api/games`);
  const data = await res.json();

  // Pass data to the page via props
  return { props: { data } };
}
