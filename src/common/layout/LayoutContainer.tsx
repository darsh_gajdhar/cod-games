import React from "react";
import { LayoutProps } from "../../../interfaces/interfaces";
import Layout from "./Layout";

const LayoutContainer = ({ children }: LayoutProps) => {
  return <Layout>{children}</Layout>;
};

export default LayoutContainer;
