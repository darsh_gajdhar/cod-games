import React from "react";
import {
  Box,
  Grid,
  Container,
  ListItemText,
  Divider,
  IconButton,
} from "@mui/material";
import Image from "next/image";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import LocalPhoneOutlinedIcon from "@mui/icons-material/LocalPhoneOutlined";
import gameLogo from "../../assests/images/game_img.png";
import FacebookOutlinedIcon from "@mui/icons-material/FacebookOutlined";
import TwitterIcon from "@mui/icons-material/Twitter";
import YouTubeIcon from "@mui/icons-material/YouTube";
import GoogleIcon from "@mui/icons-material/Google";
import InstagramIcon from "@mui/icons-material/Instagram";

import GameFooterPage, {
  GameFooterAbout,
  GameFooterAboutWrapper,
  GameFooterAddress,
  GameFooterAddressContain,
  GameFooterAddressContainer,
  GameFooterContactUs,
  GameFooterEmailAddress,
  GameFooterEmailContainer,
  GameFooterEmailWrapper,
  GameFooterExplore,
  GameFooterExploreList,
  GameFooterOurGames,
  GameFooterPhoneContainer,
  GameFooterPhoneDescription,
  OurGamesList,
  GameFooterPhoneSection,
  GameFooterImageContainer,
  GameFooterImageWrapper,
  GameFooterIcon,
  CopyRightsContainer,
  CopyRightsWrapper,
} from "./style";

const Footer: React.FC = () => {
  return (
    <footer>
      <GameFooterPage>
        <Container>
          <Grid container spacing={3}>
            <Grid item xs={6} lg={3}>
              <GameFooterAbout variant="h3">About Us</GameFooterAbout>
              <GameFooterAboutWrapper variant="subtitle1" gutterBottom>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Voluptatum odio voluptates similique totam dolor in accusamus
                adipisci cupiditate ipsa quaerat modi, earum voluptatem eos
                reiciendis, doloribus dolorum.
              </GameFooterAboutWrapper>
            </Grid>
            <Grid item xs={6} lg={3}>
              <GameFooterExplore variant="h3">Explore</GameFooterExplore>
              <GameFooterExploreList>
                <ListItemText primary=" > About" />
                <ListItemText primary=" > Our Games" />
                <ListItemText primary=" > Contact Us" />
                <ListItemText primary=" > Help & Support" />
                <ListItemText primary=" > Privacy Policy" />
              </GameFooterExploreList>
            </Grid>
            <Grid item xs={6} lg={3}>
              <GameFooterOurGames variant="h3">Our Games</GameFooterOurGames>
              <OurGamesList>
                <ListItemText primary=" > Need For Speed" />
                <ListItemText primary=" > Call Of Duty" />
                <ListItemText primary=" > Resident Evil" />
                <ListItemText primary=" > Dragon Fight" />
                <ListItemText primary=" > 2 Players Champions" />
              </OurGamesList>
            </Grid>
            <Grid item xs={6} lg={3}>
              <GameFooterContactUs variant="h3">Contact Us</GameFooterContactUs>
              <GameFooterAddressContainer>
                <LocationOnOutlinedIcon sx={{ color: "#ff7a21" }} />
                <GameFooterAddress variant="h4">Address</GameFooterAddress>
              </GameFooterAddressContainer>
              <Box>
                <GameFooterAddressContain variant="subtitle1" gutterBottom>
                  401 TimesSquare, near, Baghban Party Plot Rd, near Ravija
                  plaza, Thaltej, Ahmedabad, Gujarat 380059
                </GameFooterAddressContain>
              </Box>
              <GameFooterEmailContainer>
                <EmailOutlinedIcon sx={{ color: "#ff7a21" }} />
                <GameFooterEmailAddress variant="h4">
                  Email Address
                </GameFooterEmailAddress>
              </GameFooterEmailContainer>
              <Box>
                <GameFooterEmailWrapper variant="subtitle1" gutterBottom>
                  upforce@gmail.com
                </GameFooterEmailWrapper>
              </Box>
              <GameFooterPhoneSection>
                <LocalPhoneOutlinedIcon sx={{ color: "#ff7a21" }} />
                <GameFooterPhoneContainer variant="h4">
                  Phone
                </GameFooterPhoneContainer>
              </GameFooterPhoneSection>
              <Box>
                <GameFooterPhoneDescription variant="subtitle1" gutterBottom>
                  123-4567-890
                </GameFooterPhoneDescription>
              </Box>
            </Grid>
          </Grid>
          <Divider color="white" sx={{ marginTop: "50px" }} />
          <GameFooterImageContainer>
            <GameFooterImageWrapper>
              <Image src={gameLogo} alt="gameLogo" height={80} width={150} />
            </GameFooterImageWrapper>
            <GameFooterIcon>
              <IconButton>
                <FacebookOutlinedIcon sx={{ color: "#ff7a21" }} />
              </IconButton>
              <IconButton>
                <TwitterIcon sx={{ color: "#ff7a21" }} />
              </IconButton>
              <IconButton>
                <YouTubeIcon sx={{ color: "#ff7a21" }} />
              </IconButton>
              <IconButton>
                <GoogleIcon sx={{ color: "#ff7a21" }} />
              </IconButton>
              <IconButton>
                <InstagramIcon sx={{ color: "#ff7a21" }} />
              </IconButton>
            </GameFooterIcon>
          </GameFooterImageContainer>
          <CopyRightsContainer>
            <CopyRightsWrapper variant="h5">
              @Copyrights 2022 Avengers - All Rights Reserved
            </CopyRightsWrapper>
          </CopyRightsContainer>
        </Container>
      </GameFooterPage>
    </footer>
  );
};

export default Footer;
